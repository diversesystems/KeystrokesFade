package net.canelex.keystrokes;

import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import java.util.Iterator;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import org.apache.commons.lang3.mutable.MutableFloat;
import org.apache.commons.lang3.mutable.MutableInt;
import net.canelex.keystrokes.draggable.keystrokes.DragCPS;
import net.canelex.keystrokes.draggable.keystrokes.DragFPS;
import net.canelex.keystrokes.draggable.keystrokes.DragKeystrokes;
import net.canelex.keystrokes.draggable.DragGui;
import java.util.LinkedHashSet;
import java.io.File;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "canelexkeystrokes", name = "Canelex Keystrokes", version = "1.0")
public class KeystrokesMod
{
    private Minecraft mc;
    private File saveFile;
    private LinkedHashSet<DragGui> dragGuis;
    private DragKeystrokes keystrokes;
    private DragFPS fps;
    private DragCPS cps;
    public MutableInt bgUnpressed;
    public MutableInt textUnpressed;
    public MutableInt bgPressed;
    public MutableInt textPressed;
    public MutableInt fadingTime;
    public MutableFloat scale;
    
    public KeystrokesMod() {
        this.bgUnpressed = new MutableInt(-922746880);
        this.textUnpressed = new MutableInt(-1);
        this.bgPressed = new MutableInt(-905969665);
        this.textPressed = new MutableInt(-16777216);
        this.fadingTime = new MutableInt(100);
        this.scale = new MutableFloat(1.0f);
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        this.mc = Minecraft.getMinecraft();
        this.saveFile = new File(this.mc.mcDataDir, "canelexkeystrokes.cfg");
        (this.dragGuis = new LinkedHashSet<DragGui>()).add(this.keystrokes = new DragKeystrokes(this, 5, 5, 1.0f));
        this.dragGuis.add(this.fps = new DragFPS(this, 70, 5, 1.0f));
        this.dragGuis.add(this.cps = new DragCPS(this, 130, 5, 1.0f));
        this.loadSettings();
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandEditStrokes(this));
        MinecraftForge.EVENT_BUS.register((Object)this);
    }
    
    @SubscribeEvent
    public void onRender(final RenderGameOverlayEvent.Post event) {
        if (event.getType() == RenderGameOverlayEvent.ElementType.ALL && this.mc.currentScreen == null && !this.mc.gameSettings.showDebugInfo) {
            for (final DragGui gui : this.dragGuis) {
                gui.drawUI();
            }
        }
    }
    
    public LinkedHashSet<DragGui> getDragGuis() {
        return this.dragGuis;
    }
    
    public DragKeystrokes getKeystrokes() {
        return this.keystrokes;
    }
    
    public DragCPS getCps() {
        return this.cps;
    }
    
    public DragFPS getFps() {
        return this.fps;
    }
    
    public void saveSettings() {
        final Configuration config = new Configuration(this.saveFile);
        this.updateSettings(config, true);
        config.save();
    }
    
    public void loadSettings() {
        final Configuration config = new Configuration(this.saveFile);
        config.load();
        this.updateSettings(config, false);
        this.keystrokes.setScale(this.scale.getValue());
        this.cps.setScale(this.scale.getValue());
        this.fps.setScale(this.scale.getValue());
    }
    
    private void updateSettings(final Configuration config, final boolean save) {
        Property prop = config.get("global", "bgUnpressed", -922746880);
        if (save) {
            prop.set((int)this.bgUnpressed.getValue());
        }
        else {
            this.bgUnpressed = new MutableInt(prop.getInt());
        }
        prop = config.get("global", "bgPressed", -905969665);
        if (save) {
            prop.set((int)this.bgPressed.getValue());
        }
        else {
            this.bgPressed = new MutableInt(prop.getInt());
        }
        prop = config.get("global", "textUnpressed", -1);
        if (save) {
            prop.set((int)this.textUnpressed.getValue());
        }
        else {
            this.textUnpressed = new MutableInt(prop.getInt());
        }
        prop = config.get("global", "textPressed", -16777216);
        if (save) {
            prop.set((int)this.textPressed.getValue());
        }
        else {
            this.textPressed = new MutableInt(prop.getInt());
        }
        prop = config.get("global", "fadingtime", 100);
        if (save) {
            prop.set((int)this.fadingTime.getValue());
        }
        else {
            this.fadingTime = new MutableInt(prop.getInt());
        }
        prop = config.get("global", "scale", 1.0);
        if (save) {
            prop.set((double)(float)this.scale.getValue());
        }
        else {
            this.scale = new MutableFloat((Number)prop.getDouble());
        }
        prop = config.get("keystrokes", "mode", 0);
        if (save) {
            prop.set(this.keystrokes.getMode());
        }
        else {
            this.keystrokes.setMode(prop.getInt());
        }
        prop = config.get("keystrokes", "posX", 5);
        if (save) {
            prop.set(this.keystrokes.getPosX());
        }
        else {
            this.keystrokes.setPosX(prop.getInt());
        }
        prop = config.get("keystrokes", "posY", 5);
        if (save) {
            prop.set(this.keystrokes.getPosY());
        }
        else {
            this.keystrokes.setPosY(prop.getInt());
        }
        prop = config.get("cpsmod", "enabled", true);
        if (save) {
            prop.set(this.cps.isEnabled());
        }
        else {
            this.cps.setEnabled(prop.getBoolean());
        }
        prop = config.get("cpsmod", "posX", 5);
        if (save) {
            prop.set(this.cps.getPosX());
        }
        else {
            this.cps.setPosX(prop.getInt());
        }
        prop = config.get("cpsmod", "posY", 50);
        if (save) {
            prop.set(this.cps.getPosY());
        }
        else {
            this.cps.setPosY(prop.getInt());
        }
        prop = config.get("fpsmod", "enabled", true);
        if (save) {
            prop.set(this.fps.isEnabled());
        }
        else {
            this.fps.setEnabled(prop.getBoolean());
        }
        prop = config.get("fpsmod", "posX", 5);
        if (save) {
            prop.set(this.fps.getPosX());
        }
        else {
            this.fps.setPosX(prop.getInt());
        }
        prop = config.get("fpsmod", "posY", 75);
        if (save) {
            prop.set(this.fps.getPosY());
        }
        else {
            this.fps.setPosY(prop.getInt());
        }
    }
}
