package net.canelex.keystrokes.draggable.keystrokes;

import java.util.Iterator;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.settings.GameSettings;
import net.canelex.keystrokes.KeystrokesMod;
import java.util.LinkedHashSet;
import net.canelex.keystrokes.draggable.DragGui;

public class DragKeystrokes extends DragGui
{
    private int mode;
    private LinkedHashSet<GuiKey> keySet;
    
    public DragKeystrokes(final KeystrokesMod mod, final int x, final int y, final float scale) {
        super(mod, x, y, scale);
        final GameSettings gs = this.mc.gameSettings;
        (this.keySet = new LinkedHashSet<GuiKey>()).add(new GuiKey(mod, 20, 0, 19, 19, gs.keyBindForward));
        this.keySet.add(new GuiKey(mod, 0, 20, 19, 19, gs.keyBindLeft));
        this.keySet.add(new GuiKey(mod, 20, 20, 19, 19, gs.keyBindBack));
        this.keySet.add(new GuiKey(mod, 40, 20, 19, 19, gs.keyBindRight));
        this.keySet.add(new GuiKey(mod, 0, 40, 29, 19, gs.keyBindAttack));
        this.keySet.add(new GuiKey(mod, 30, 40, 29, 19, gs.keyBindUseItem));
        this.keySet.add(new GuiKeySpace(mod, 0, 60, 59, 11, gs.keyBindJump));
    }
    
    @Override
    public void drawUI() {
        if (this.scale != 0.0f) {
            GL11.glTranslatef((float)(-this.posX) * (this.scale - 1.0f), (float)(-this.posY) * (this.scale - 1.0f), 0.0f);
            GL11.glScalef(this.scale, this.scale, 1.0f);
            final Iterator<GuiKey> iteratorKeys = this.keySet.iterator();
            for (int numKeysToDraw = (this.mode == 0) ? 4 : ((this.mode == 1) ? 6 : 7), i = 0; i < numKeysToDraw; ++i) {
                final GuiKey key = (GuiKey)iteratorKeys.next();
                key.updateKeyStates();
                key.drawKey(this.posX, this.posY);
            }
            GL11.glScalef(1.0f / this.scale, 1.0f / this.scale, 1.0f);
            GL11.glTranslatef((float)this.posX * (this.scale - 1.0f), (float)this.posY * (this.scale - 1.0f), 0.0f);
        }
    }
    
    @Override
    public int getWidth() {
        return 59;
    }
    
    @Override
    public int getHeight() {
        switch (this.mode) {
            case 0: {
                return 40;
            }
            case 1: {
                return 60;
            }
            default: {
                return 72;
            }
        }
    }
    
    public int getMode() {
        return this.mode;
    }
    
    public void setMode(final int mode) {
        this.mode = mode;
    }
}
