package net.canelex.keystrokes.draggable.keystrokes;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import org.lwjgl.opengl.GL11;
import net.canelex.keystrokes.KeystrokesMod;
import net.canelex.keystrokes.draggable.DragGui;

public class DragFPS extends DragGui
{
    public boolean enabled;
    
    public DragFPS(final KeystrokesMod mod, final int x, final int y, final float scale) {
        super(mod, x, y, scale);
        this.enabled = true;
    }
    
    @Override
    public void drawUI() {
        if (!this.enabled) {
            return;
        }
        if (this.scale != 0.0f) {
            final FontRenderer fr = this.mc.fontRendererObj;
            final String sToDraw = this.mc.debug.split(" ")[0] + " FPS";
            GL11.glTranslatef((float)(-this.posX) * (this.scale - 1.0f), (float)(-this.posY) * (this.scale - 1.0f), 0.0f);
            GL11.glScalef(this.scale, this.scale, 1.0f);
            Gui.drawRect(this.posX, this.posY, this.posX + this.getWidth(), this.posY + this.getHeight(), (int)this.mod.bgUnpressed.getValue());
            final int mx = this.posX + (this.getWidth() - fr.getStringWidth(sToDraw)) / 2;
            final int my = this.posY + (this.getHeight() - fr.FONT_HEIGHT) / 2 + 1;

            fr.drawString(sToDraw, mx, my, (int)this.mod.textUnpressed.getValue());

            GL11.glScalef(1.0f / this.scale, 1.0f / this.scale, 1.0f);
            GL11.glTranslatef((float)this.posX * (this.scale - 1.0f), (float)this.posY * (this.scale - 1.0f), 0.0f);
        }
    }
    
    @Override
    public int getWidth() {
        return 59;
    }
    
    @Override
    public int getHeight() {
        return 14;
    }
    
    public boolean isEnabled() {
        return this.enabled;
    }
    
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
}
