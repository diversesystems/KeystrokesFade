package net.canelex.keystrokes.draggable.keystrokes;

import net.minecraft.client.settings.KeyBinding;
import net.canelex.keystrokes.KeystrokesMod;

public class GuiKeySpace extends GuiKey
{
    public GuiKeySpace(final KeystrokesMod mod, final int x, final int y, final int width, final int height, final KeyBinding keyBinding) {
        super(mod, x, y, width, height, keyBinding);
    }
    
    @Override
    public void drawKey(int x, int y) {
        x += this.relX;
        y += this.relY;
        drawRect(x, y, x + this.width, y + this.height, this.getBackgroundColor());
        this.drawHorizontalLine(x + this.width / 2 - 6, x + this.width / 2 + 6, y + this.height / 2 - 1, this.getTextColor());
    }
}
