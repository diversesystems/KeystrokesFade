package net.canelex.keystrokes.draggable.keystrokes;

import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraft.client.gui.FontRenderer;
import java.util.Iterator;
import net.minecraft.client.gui.Gui;
import org.lwjgl.opengl.GL11;
import net.minecraftforge.common.MinecraftForge;
import net.canelex.keystrokes.KeystrokesMod;
import io.netty.util.internal.ConcurrentSet;
import net.canelex.keystrokes.draggable.DragGui;

public class DragCPS extends DragGui
{
    private ConcurrentSet<Long> clicks;
    private boolean enabled;
    
    public DragCPS(final KeystrokesMod mod, final int x, final int y, final float scale) {
        super(mod, x, y, scale);
        this.clicks = (ConcurrentSet<Long>)new ConcurrentSet();
        this.enabled = true;
        MinecraftForge.EVENT_BUS.register((Object)this);
    }
    
    @Override
    public void drawUI() {
        if (!this.enabled) {
            return;
        }
        for (final Long click : this.clicks) {
            if (System.currentTimeMillis() - 1000L > click) {
                this.clicks.remove((Object)click);
            }
        }
        if (this.scale != 0.0f) {
            final FontRenderer fr = this.mc.fontRendererObj;
            final String sToDraw = this.clicks.size() + " CPS";
            GL11.glTranslatef((float)(-this.posX) * (this.scale - 1.0f), (float)(-this.posY) * (this.scale - 1.0f), 0.0f);
            GL11.glScalef(this.scale, this.scale, 1.0f);
            Gui.drawRect(this.posX, this.posY, this.posX + this.getWidth(), this.posY + this.getHeight(), (int)this.mod.bgUnpressed.getValue());
            final int mx = this.posX + (this.getWidth() - fr.getStringWidth(sToDraw)) / 2;
            final int my = this.posY + (this.getHeight() - fr.FONT_HEIGHT) / 2 + 1;

            fr.drawString(sToDraw, mx, my, (int)this.mod.textUnpressed.getValue());

            GL11.glScalef(1.0f / this.scale, 1.0f / this.scale, 1.0f);
            GL11.glTranslatef((float)this.posX * (this.scale - 1.0f), (float)this.posY * (this.scale - 1.0f), 0.0f);
        }
    }
    
    @SubscribeEvent
    public void onInput(final MouseEvent event) {
        if (event.isButtonstate())
            this.clicks.add(System.currentTimeMillis());
    }
    
    @Override
    public int getWidth() {
        return 59;
    }
    
    @Override
    public int getHeight() {
        return 14;
    }
    
    public boolean isEnabled() {
        return this.enabled;
    }
    
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
}
