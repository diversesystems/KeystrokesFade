package net.canelex.keystrokes.draggable.keystrokes;

import net.canelex.keystrokes.gui.GuiEditKeystrokes;
import net.canelex.keystrokes.gui.GuiEditColor;
import java.awt.Color;
import org.lwjgl.input.Mouse;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.settings.KeyBinding;
import net.canelex.keystrokes.KeystrokesMod;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;

public class GuiKey extends Gui
{
    protected final Minecraft mc;
    protected final FontRenderer fr;
    protected final KeystrokesMod mod;
    protected int relX;
    protected int relY;
    protected int width;
    protected int height;
    protected KeyBinding keyBinding;
    protected boolean isPressed;
    protected float percentFaded;
    private long lastKeyUpdate;
    
    public GuiKey(final KeystrokesMod mod, final int x, final int y, final int width, final int height, final KeyBinding keyBinding) {
        this.mc = Minecraft.getMinecraft();
        this.fr = this.mc.fontRendererObj;
        this.mod = mod;
        this.relX = x;
        this.relY = y;
        this.width = width;
        this.height = height;
        this.keyBinding = keyBinding;
        this.percentFaded = 0.0f;
        this.lastKeyUpdate = System.currentTimeMillis();
    }
    
    public void drawKey(int x, int y) {
        x += this.relX;
        y += this.relY;
        drawRect(x, y, x + this.width, y + this.height, this.getBackgroundColor());
        x += (this.width - this.fr.getStringWidth(this.getKeyName())) / 2;
        y += (this.height - this.fr.FONT_HEIGHT) / 2 + 1;

        this.fr.drawString(this.getKeyName(), x + 1, y, this.getTextColor());

    }
    
    protected int getBackgroundColor() {
        final int thisColor = (int)(this.isPressed ? this.mod.bgPressed.getValue() : this.mod.bgUnpressed.getValue());
        final int lastColor = (int)(this.isPressed ? this.mod.bgUnpressed.getValue() : this.mod.bgPressed.getValue());
        return this.getColor(thisColor, lastColor);
    }
    
    protected int getTextColor() {
        final int thisColor = (int)(this.isPressed ? this.mod.textPressed.getValue() : this.mod.textUnpressed.getValue());
        final int lastColor = (int)(this.isPressed ? this.mod.textUnpressed.getValue() : this.mod.textPressed.getValue());
        return this.getColor(thisColor, lastColor);
    }
    
    private String getKeyName() {
        final int code = this.keyBinding.getKeyCode();
        switch (code) {
            case -100: {
                return "LMB";
            }
            case -99: {
                return "RMB";
            }
            case -98: {
                return "MMB";
            }
            case 200: {
                return "U";
            }
            case 203: {
                return "L";
            }
            case 205: {
                return "R";
            }
            case 208: {
                return "D";
            }
            default: {
                if (code >= 0 && code <= 223) {
                    return Keyboard.getKeyName(code);
                }
                if (code >= -100 && code <= -84) {
                    return Mouse.getButtonName(code + 100);
                }
                return "[]";
            }
        }
    }
    
    private int getColor(final int thisColor, final int lastColor) {
        final Color c1 = new Color(thisColor, true);
        final Color c2 = new Color(lastColor, true);
        int r = (int)Math.ceil(c1.getRed() * this.percentFaded) + (int)Math.floor(c2.getRed() * (1.0f - this.percentFaded));
        int g = (int)Math.ceil(c1.getGreen() * this.percentFaded) + (int)Math.floor(c2.getGreen() * (1.0f - this.percentFaded));
        int b = (int)Math.ceil(c1.getBlue() * this.percentFaded) + (int)Math.floor(c2.getBlue() * (1.0f - this.percentFaded));
        int a = (int)Math.ceil(c1.getAlpha() * this.percentFaded) + (int)Math.floor(c2.getAlpha() * (1.0f - this.percentFaded));
        r = Math.max(0, Math.min(255, r));
        g = Math.max(0, Math.min(255, g));
        b = Math.max(0, Math.min(255, b));
        a = Math.max(0, Math.min(255, a));
        return new Color(r, g, b, a).getRGB();
    }
    
    public void updateKeyStates() {
        if (this.percentFaded < 1.0f) {
            this.percentFaded += (float) (System.currentTimeMillis() - this.lastKeyUpdate) / this.mod.fadingTime.getValue();
            if (this.percentFaded > 1.0f) {
                this.percentFaded = 1.0f;
            }
        }
        if (this.mc.currentScreen instanceof GuiEditColor) {
            this.isPressed = ((GuiEditColor)this.mc.currentScreen).keyPressed;
        }
        else if (this.mc.currentScreen instanceof GuiEditKeystrokes) {
            final boolean pressed = System.currentTimeMillis() % 1000L > 500L;
            if (pressed != this.isPressed) {
                this.isPressed = pressed;
                this.percentFaded = 0.0f;
            }
        }
        else if (this.keyBinding.isKeyDown() != this.isPressed) {
            this.isPressed = this.keyBinding.isKeyDown();
            this.percentFaded = 0.0f;
        }
        if (this.percentFaded < 0.0f) {
            this.percentFaded = 0.0f;
        }
        this.lastKeyUpdate = System.currentTimeMillis();
    }
}
