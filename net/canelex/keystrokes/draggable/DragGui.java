package net.canelex.keystrokes.draggable;

import net.canelex.keystrokes.KeystrokesMod;
import net.minecraft.client.Minecraft;

public abstract class DragGui
{
    protected Minecraft mc;
    protected KeystrokesMod mod;
    protected int posX;
    protected int posY;
    protected float scale;
    
    public DragGui(final KeystrokesMod mod, final int x, final int y, final float scale) {
        this.mc = Minecraft.getMinecraft();
        this.mod = mod;
        this.posX = x;
        this.posY = y;
        this.scale = scale;
    }
    
    public int getPosX() {
        return this.posX;
    }
    
    public void setPosY(final int posY) {
        this.posY = posY;
    }
    
    public int getPosY() {
        return this.posY;
    }
    
    public void setPosX(final int posX) {
        this.posX = posX;
    }
    
    public float getScale() {
        return this.scale;
    }
    
    public void setScale(final float scale) {
        this.scale = scale;
    }
    
    public abstract int getWidth();
    
    public abstract int getHeight();
    
    public abstract void drawUI();
}
