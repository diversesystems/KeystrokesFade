package net.canelex.keystrokes;

import net.minecraft.command.CommandException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import net.canelex.keystrokes.gui.GuiEditKeystrokes;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;

public class CommandEditStrokes extends CommandBase
{
    private KeystrokesMod mod;
    
    public CommandEditStrokes(final KeystrokesMod mod) {
        this.mod = mod;
    }
    
    public String getCommandName() {
        return "keystrokes";
    }
    
    public String getCommandUsage(final ICommandSender sender) {
        return "/keystrokes";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        MinecraftForge.EVENT_BUS.register((Object)this);
    }

    public boolean func_71519_b(final ICommandSender sender) {
        return true;
    }

    
    @SubscribeEvent
    public void onTick(final TickEvent.ClientTickEvent event) {
        MinecraftForge.EVENT_BUS.unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GuiEditKeystrokes(this.mod));
    }
}
