package net.canelex.keystrokes.gui;

import java.awt.Color;
import net.minecraft.client.gui.GuiButton;
import net.canelex.keystrokes.KeystrokesMod;
import net.minecraft.client.gui.GuiScreen;
import org.apache.commons.lang3.mutable.MutableInt;

public class GuiEditColor extends GuiScreenDrag
{
    public boolean keyPressed;
    public MutableInt changedColor;
    private GuiScreen last;
    private GuiSlider red;
    private GuiSlider green;
    private GuiSlider blue;
    private GuiSlider alpha;
    
    public GuiEditColor(final KeystrokesMod mod, final MutableInt mutableInt, final GuiScreen last, final boolean keyPressed) {
        super(mod);
        this.changedColor = mutableInt;
        this.last = last;
        this.keyPressed = keyPressed;
    }
    
    @Override
    public void initGui() {
        super.initGui();
        final int rgba = (int)this.changedColor.getValue();
        this.buttonList.add(this.red = new GuiSlider(0, this.width / 2 - 75, this.height / 2 - 50, 150, 20, "Red: ", "", 0.0, 255.0, rgba >> 16 & 0xFF, false, true));
        this.buttonList.add(this.green = new GuiSlider(1, this.width / 2 - 75, this.height / 2 - 25, 150, 20, "Green: ", "", 0.0, 255.0, rgba >> 8 & 0xFF, false, true));
        this.buttonList.add(this.blue = new GuiSlider(2, this.width / 2 - 75, this.height / 2, 150, 20, "Blue: ", "", 0.0, 255.0, rgba & 0xFF, false, true));
        this.buttonList.add(this.alpha = new GuiSlider(3, this.width / 2 - 75, this.height / 2 + 25, 150, 20, "Alpha: ", "", 10.0, 255.0, rgba >> 24 & 0xFF, false, true));
        this.buttonList.add(new GuiButton(4, this.width / 2 - 75, this.height / 2 + 50, 150, 20, "Done"));
    }
    
    @Override
    public void drawScreen(final int x, final int y, final float partial) {
        this.drawDefaultBackground();
        super.drawScreen(x, y, partial);
        this.updateColor();
    }
    
    protected void actionPerformed(final GuiButton button) {
        if (button.id == 4) {
            this.mc.displayGuiScreen(this.last);
        }
    }
    
    private void updateColor() {
        final int r = this.red.getValueInt();
        final int g = this.green.getValueInt();
        final int b = this.blue.getValueInt();
        final int a = this.alpha.getValueInt();
        this.changedColor.setValue(new Color(r, g, b, a).getRGB());
    }
}
