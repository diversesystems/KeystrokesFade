package net.canelex.keystrokes.gui;

import net.minecraft.client.gui.GuiScreen;
import java.util.Iterator;
import net.canelex.keystrokes.draggable.DragGui;
import net.minecraft.client.gui.GuiButton;
import net.canelex.keystrokes.KeystrokesMod;
import net.canelex.keystrokes.draggable.keystrokes.DragKeystrokes;

public class GuiEditKeystrokes extends GuiScreenDrag
{
    private DragKeystrokes keystrokes;
    
    public GuiEditKeystrokes(final KeystrokesMod mod) {
        super(mod);
        this.keystrokes = mod.getKeystrokes();
    }
    
    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 105, this.height / 2 - 75, 103, 20, "BG [Key Up]"));
        this.buttonList.add(new GuiButton(1, this.width / 2 + 3, this.height / 2 - 75, 103, 20, "Text [Key Up]"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 105, this.height / 2 - 50, 103, 20, "BG [Key Down]"));
        this.buttonList.add(new GuiButton(3, this.width / 2 + 3, this.height / 2 - 50, 103, 20, "Text [Key Down]"));
        this.buttonList.add(new GuiButton(4, this.width / 2 - 105, this.height / 2 - 25, 210, 20, "Change Mode"));
        this.buttonList.add(new GuiSlider(5, this.width / 2 - 105, this.height / 2, 210, 20, "Fade Time: ", "", 1.0, 500.0, (double)this.mod.fadingTime.getValue(), false, true));
        this.buttonList.add(new GuiSlider(6, this.width / 2 - 105, this.height / 2 + 25, 210, 20, "Scale: ", "%", 0.0, 200.0, this.mod.scale.getValue() * 100.0, false, true));
        this.buttonList.add(new GuiButton(7, this.width / 2 - 105, this.height / 2 + 50, 103, 20, "Toggle CPS"));
        this.buttonList.add(new GuiButton(8, this.width / 2 + 3, this.height / 2 + 50, 103, 20, "Toggle FPS"));
    }
    
    @Override
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.mod.fadingTime.setValue(((GuiSlider)this.buttonList.get(5)).getValueInt());
        this.mod.scale.setValue((float)((GuiSlider)this.buttonList.get(6)).getValueInt() / 100.0f);
        for (final DragGui gui : this.mod.getDragGuis()) {
            gui.setScale(this.mod.scale.getValue());
        }
    }
    
    protected void actionPerformed(final GuiButton button) {
        switch (button.id) {
            case 0: {
                this.mc.displayGuiScreen((GuiScreen)new GuiEditColor(this.mod, this.mod.bgUnpressed, this, false));
                break;
            }
            case 1: {
                this.mc.displayGuiScreen((GuiScreen)new GuiEditColor(this.mod, this.mod.textUnpressed, this, false));
                break;
            }
            case 2: {
                this.mc.displayGuiScreen((GuiScreen)new GuiEditColor(this.mod, this.mod.bgPressed, this, true));
                break;
            }
            case 3: {
                this.mc.displayGuiScreen((GuiScreen)new GuiEditColor(this.mod, this.mod.textPressed, this, true));
                break;
            }
            case 4: {
                this.keystrokes.setMode((this.keystrokes.getMode() + 1) % 3);
                break;
            }
            case 7: {
                this.mod.getCps().setEnabled(!this.mod.getCps().isEnabled());
                break;
            }
            case 8: {
                this.mod.getFps().setEnabled(!this.mod.getFps().isEnabled());
                break;
            }
        }
    }
    
    public void onGuiClosed() {
        this.mod.saveSettings();
    }
}
