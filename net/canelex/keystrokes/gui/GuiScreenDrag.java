package net.canelex.keystrokes.gui;

import java.io.IOException;
import java.util.Iterator;
import net.canelex.keystrokes.draggable.DragGui;
import net.canelex.keystrokes.KeystrokesMod;
import net.minecraft.client.gui.GuiScreen;

public class GuiScreenDrag extends GuiScreen
{
    protected KeystrokesMod mod;
    protected DragGui draggedGui;
    protected int lastX;
    protected int lastY;
    
    public GuiScreenDrag(final KeystrokesMod mod) {
        this.mod = mod;
    }
    
    public void initGui() {
        for (final DragGui gui : this.mod.getDragGuis()) {
            this.fitGuiIntoScreen(gui);
        }
    }
    
    public void drawScreen(final int x, final int y, final float partial) {
        super.drawScreen(x, y, partial);
        for (final DragGui gui : this.mod.getDragGuis()) {
            gui.drawUI();
        }
        if (this.draggedGui != null) {
            this.draggedGui.setPosX(this.draggedGui.getPosX() + x - this.lastX);
            this.draggedGui.setPosY(this.draggedGui.getPosY() + y - this.lastY);
            this.fitGuiIntoScreen(this.draggedGui);
        }
        this.lastX = x;
        this.lastY = y;
    }
    
    protected void mouseClicked(final int x, final int y, final int button) throws IOException {
        super.mouseClicked(x, y, button);
        for (final DragGui gui : this.mod.getDragGuis()) {
            if (x >= gui.getPosX() && y >= gui.getPosY() && x <= gui.getPosX() + gui.getWidth() * gui.getScale() && y <= gui.getPosY() + gui.getHeight() * gui.getScale()) {
                this.draggedGui = gui;
                this.lastX = x;
                this.lastY = y;
                break;
            }
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        super.mouseReleased(mouseX, mouseY, state);
        this.draggedGui = null;
    }
    
    private void fitGuiIntoScreen(final DragGui gui) {
        gui.setPosX(Math.min(this.width - (int)(gui.getWidth() * gui.getScale()), Math.max(0, gui.getPosX())));
        gui.setPosY(Math.min(this.height - (int)(gui.getHeight() * gui.getScale()), Math.max(0, gui.getPosY())));
    }
}
